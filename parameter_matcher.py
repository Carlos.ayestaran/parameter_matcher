#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 17:20:53 2018

@author: carlos
"""


import configparser
import numpy as np
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove
from scipy.optimize import least_squares as least_sq
from scipy.optimize import minimize as minimize
from scipy.optimize import basinhopping as annealing
import subprocess
from pyswarm import pso
from configparser import ConfigParser, BasicInterpolation
import ast

#subprocess.call(["lmp_serial1 -l test1.lammps -in Opt_HA_3x3_str1_2.in"],shell=True)
#/usr/local/Cellar/lammps/2017-08-11_4/bin/lmp_serial
#mpirun -np 20 lammps-daily  <  frame_100.in 


def readdefault(inputfile):
    #returns the values in the default inputfile, which should have 5 lines: 
    #parameters, default values, filenames, dft per molecule and number of 
    #molecules
    f=open(inputfile)
    lines=f.readlines()
    f.close()
    #finde index of the separatorline
    for line in lines:
        if '#' in line:
            index=lines.index(line)
    #Recover the files, energies and Nmol from the 1st half
    files=[]
    for x in lines[0:index]:
        files.append(x.split()[:])
    filenames=[list[0]for list in files]
    eners=np.asfarray([list[1]for list in files])
    Nmol=np.asfarray([list[2]for list in files])
    includefiles=[list[3]for list in files]
    fileweights=np.asfarray([list[4]for list in files])
    #Recover the param_names, param_values, lb and ub from 2nd half
    params=[]
    for x in lines[index+1:]:
        params.append(x.split()[:])
    param_names=[list[0]for list in params]
    param_values=np.asfarray([list[1]for list in params])
    lb=np.asfarray([list[2]for list in params])
    ub=np.asfarray([list[3]for list in params])
    paramweights=np.asfarray([list[4]for list in params])
    return param_names,param_values,lb, ub, filenames, eners, Nmol,includefiles,fileweights,paramweights
        
def readconfig(filename):
    config=ConfigParser(interpolation=BasicInterpolation())
    config.read('config_test.in')
    prefix=config.get('user','prefix')
    logfile=config.get('user','logfile')
    outputfile=config.get('user','outputfile')
    path=config.get('user','configurations_file')
    file=open(path,'r+')
    lines=file.readlines()
    file.close()   
    param_names=lines[0].split()
    param_values=np.asfarray(lines[1].strip('\n').split())
    lb=dft=np.asfarray(lines[2].strip('\n').split())
    ub=np.asfarray(lines[3].strip('\n').split())
    filenames=lines[4].split()
    dft=np.asfarray(lines[5].strip('\n').split())
    return prefix, logfile, outputfile, path, param_names, param_values, lb, ub, filenames, dft

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

def paster (filename,patternfile,ID='Pastehere'):
    #function to paste the text in patternfile into the filename text file at
    #the position with the ID
    with open(filename, "r") as file:
        filelines = file.readlines()
        file.close()
    with open(patternfile, "r") as paste:
        pastelines = paste.readlines()
        paste.close()
    for line in filelines:
        if ID in line:
            index=filelines.index(line)
            break
    newlines=list()
    newlines.extend(filelines[0:index])
    newlines.extend(pastelines)
    newlines.extend(filelines[index+1:len(filelines)])
    with open(filename,'w') as file:
        file.writelines(newlines)
        file.close()
def paramreplacer(filename,param,value):
    #takes file filename, looks for the first line with 'param' (aimed to be
    #variable param equal value) and replaces the value
    with open(filename, "r") as file:
        filelines = file.readlines()
        file.close()
    for line in filelines:
        if param in line:
            index=filelines.index(line)
            break
    subst=line.split()
    subst[-1]=str(value)
    subst=' '.join(subst)
    subst=subst+ '\n'
    replace(filename,line,subst)
    
def sensitivity_est(configfile):
    """Sort parameters by relative correlation to the energies. See 
    documentation for more information..
    """
    
    #Gives an estimation of how the total energy of the files is change by
    #each parameter. 
    
    #read the configfile an inputfile
    config=ConfigParser(interpolation=BasicInterpolation())
    config.read(configfile)
    prefix=config.get('user','prefix')
    logfile=config.get('user','logfile')
    Ename=config.get('user','energyname')
    inputfile=config.get('user','inputfile')
    param_names,param_values,lb, ub, filenames, eners, Nmol,includefiles,fileweights,paramweights=readdefault(inputfile)
    
    #initialize arrays to store the default energies and the variations
    defaultE=np.zeros(len(filenames))
    param_variations=np.zeros(len(param_names))
    
    #The following stores the energies with the default parameters for each file
    for i in range(len(includefiles)):
        for param in param_names:
            paramreplacer(includefiles[i],param,param_values[param_names.index(param)])
        run_command=prefix+ ' '+ filenames[i]
        subprocess.call(run_command,shell=True)
        defaultE[i]=getmethevalue(logfile,Ename)/Nmol[i]
        
    #Here goes the part that changes each parameter for each file
    
    #go through each parameter
    for param in param_names:
        print('Dealing with param:')
        print(param)
        
        #go through each file and store the new energy after variating a parameter
        for j in range(len(filenames)):
            print('Dealing with file:')
            print(filenames[j])
            
            #change the includefile for the LAMMPS input to include the 
            #corresponding param variation
            paramreplacer(includefiles[j],param,param_values[param_names.index(param)]+0.1)  
            
            #run and store the variation
            run_command=prefix+ ' '+ filenames[j]
            subprocess.call(run_command,shell=True)
            variation=(defaultE[j]-(getmethevalue(logfile,Ename)/Nmol[j]))/defaultE[j]
            
            #calculate the relative variation and make it abs
            variation=variation/((0.1-param_values[param_names.index(param)])/np.max([0.1,param_values[param_names.index(param)]]))
            variation=np.abs(variation)
            
            #add all the variations for all the filenames for each parameter
            param_variations[param_names.index(param)]=param_variations[param_names.index(param)]+variation
            #print('Updated total variation:')
            #print(param_variations)
            
            #restore the includefile
            paramreplacer(includefiles[j],param,param_values[param_names.index(param)])
    sort_index = np.flip(np.argsort(param_variations),axis=0)
    sorted_params=[param_names[i] for i in sort_index]
    param_variations=param_variations/len(filenames)
    param_variations=np.flip(np.sort(param_variations),axis=0)
    print('Recommended sorting is\n')
    print(sorted_params)
    print('With associated variations\n')
    print(param_variations)
    return sorted_params,param_variations
    
        
def getmethevalue(filename,variable):
    file=open(filename)
    value=[]
    lines=file.readlines()
    file.close()
    for line in lines:
        counter=0 #counter for picking the value of c_adsenertot from the line string
        
        for part in line.split():
            counter=counter+1
            
            if "=" in line.split(): #check first if there's an '=' in that line, because there's an extra line that includes c_adsenertot, but has no '='
                if variable in part:
                    counter=counter+1
                    #print(counter)
                    #print(line.split())
                    value.append(line.split()[counter])
                    #print(part)
    value=np.array(value).astype(np.float)
    return value


def costfunction(param_values,configfile):
    #extract prefix, 
    config=ConfigParser(interpolation=BasicInterpolation()) 
    config.read(configfile)
    prefix=config.get('user','prefix')
    logfile=config.get('user','logfile')
    inputfile=config.get('user','inputfile')
    energyname=config.get('user','energyname')
    
    #extract stuff from the inputfile
    param_names,default_param_values,lb, ub, filenames, dft_ener, Nmol,includefiles, fileweights,paramweights=readdefault(inputfile)
    
    print("Parameters:")
    print(param_names)
    print("Values:")
    print(param_values)
    #test_lines are the ones that are going to be searched and changed in the
    #includefiles
    test_lines=list()
    for i in range(len(param_names)):
        test_lines.append('variable ' +param_names[i]+ ' equal ')
        
    #initialize energy array
    ener=np.zeros(len(filenames))

    
    #go through each LAMMPS file input (filenames)
    for j in range(len(filenames)):
        f=open(includefiles[j])
        defaultlines=f.readlines()
        lines=defaultlines.copy()
        f.close()
        
        #replace the parameter values in the corresponding includefile with 
        #the corresponding param_value
        for i in range(len(param_values)):
            param_value=param_values[i]
            for k in range(len(lines)):
                if test_lines[i] in lines[k]:
                    lines[k]=test_lines[i]+str(param_value)+'\n'
                    
        #replace the includefile with the new lines
        file=open(includefiles[j],'w+')
        file.writelines(lines)
        file.close()
        
        #run LAMMPS
        run_command=prefix+ ' '+ filenames[j]
        subprocess.call(run_command,shell=True)
        
        #restore includefile
        file=open(includefiles[j],'w+')
        file.writelines(defaultlines)
        file.close()
        #get the energy value from the lofgile
        ener[j]=getmethevalue(logfile,energyname)/Nmol[j]
    
    #calculate cost
    cost=np.sum(fileweights*(ener-dft_ener)**2)+np.sum(paramweights*(param_values-default_param_values)**2)
    
    #calculate residuals
    new_vars=np.concatenate((ener,param_values))
    old_vars=np.concatenate((dft_ener,default_param_values))
    weights=np.concatenate((fileweights,paramweights))
    resids=weights*((new_vars-old_vars))

    print('cost=')
    print(cost)
    print('residuals=')
    print(resids)
    return cost

def residuals(param_values,configfile):
    #extract prefix, 
    config=ConfigParser(interpolation=BasicInterpolation()) 
    config.read(configfile)
    prefix=config.get('user','prefix')
    logfile=config.get('user','logfile')
    inputfile=config.get('user','inputfile')
    energyname=config.get('user','energyname')
    
    #extract stuff from the inputfile
    param_names,default_param_values,lb, ub, filenames, dft_ener, Nmol,includefiles, fileweights,paramweights=readdefault(inputfile)
    
    print("Parameters:")
    print(param_names)
    print("Values:")
    print(param_values)
    #test_lines are the ones that are going to be searched and changed in the
    #includefiles
    test_lines=list()
    for i in range(len(param_names)):
        test_lines.append('variable ' +param_names[i]+ ' equal ')
        
    #initialize energy array
    ener=np.zeros(len(filenames))

    
    #go through each LAMMPS file input (filenames)
    for j in range(len(filenames)):
        f=open(includefiles[j])
        defaultlines=f.readlines()
        lines=defaultlines.copy()
        f.close()
        
        #replace the parameter values in the corresponding includefile with 
        #the corresponding param_value
        for i in range(len(param_values)):
            param_value=param_values[i]
            for k in range(len(lines)):
                if test_lines[i] in lines[k]:
                    lines[k]=test_lines[i]+str(param_value)+'\n'
                    
        #replace the includefile with the new lines
        file=open(includefiles[j],'w+')
        file.writelines(lines)
        file.close()
        
        #run LAMMPS
        run_command=prefix+ ' '+ filenames[j]
        subprocess.call(run_command,shell=True)
        
        #restore includefile
        file=open(includefiles[j],'w+')
        file.writelines(defaultlines)
        file.close()
        #get the energy value from the lofgile
        ener[j]=getmethevalue(logfile,energyname)/Nmol[j]
    
    #calculate cost
    cost=np.sum(fileweights*(ener-dft_ener)**2)+np.sum(paramweights*(param_values-default_param_values)**2)
    
    #calculate residuals
    new_vars=np.concatenate((ener,param_values))
    old_vars=np.concatenate((dft_ener,default_param_values))
    weights=np.concatenate((fileweights,paramweights))
    resids=weights*((new_vars-old_vars))

    print('cost=')
    print(cost)
    print('residuals=')
    print(resids)
    return resids


def matcher(configfile):
    """Perform the minimization as indicated in the configfile. See 
    documentation for explanation. 
    """
    #read config file
    config=ConfigParser(interpolation=BasicInterpolation()) 
    config.read(configfile)
    inputfile=config.get('user','inputfile')
    param_names,param_values,lb, ub, filenames, eners, Nmol,includefiles,fileweights,paramweights=readdefault(inputfile)
    
    mininpt=config['minimization']
    method=mininpt['method']
    minopts=mininpt['minimizer_options']
    minopts=ast.literal_eval(minopts)
    
    #Read annealing input and boolean to determine whether minimization is done
    
    annealinginpt=config['annealing']
    annealingtest=annealinginpt.getboolean('annealing')
    
    psoinpt=config['particle swarm']
    psotest=psoinpt.getboolean('pso')
    psoopts=config['particle swarm']
    
    
    #define a bounds tuple for the minimizers that take it this way
    bnds=list()
    for i in range(len(param_names)):
        bnds.append([lb[i],ub[i]])
    bnds=tuple(bnds)
    
    #auxiliary function for the trf method, not included in scipy.optimize.minimize
    #but included in scipy.optimize.least_squares
    def myleastsq(fun,x0,args,bounds,diff_step,max_nfev,callback=None,**options):
        result=least_sq(fun,x0, args=args,bounds=bounds,
                        diff_step=diff_step,max_nfev=max_nfev)
        return result
    
    #trf needs an auxiliary function
    if annealingtest:
        
        #define coustom step taking routine to respect the bounds
        class RandomDisplacementBounds(object):
            """random displacement with bounds"""
            def __init__(self, xmin, xmax, stepsize=0.5):
                self.xmin = xmin
                self.xmax = xmax
                self.stepsize = stepsize
        
            def __call__(self, x):
                """take a random step but ensure the new position is within the bounds"""
                while True:
                    # this could be done in a much more clever way, but it will work for example purposes
                    xnew = x + np.random.uniform(-self.stepsize, self.stepsize, np.shape(x))
                    if np.all(xnew < self.xmax) and np.all(xnew > self.xmin):
                        break
                return xnew
        bounded_step = RandomDisplacementBounds(np.array([b[0] for b in bnds]),
                                                np.array([b[1] for b in bnds]),
                                                stepsize=annealinginpt.getfloat('stepsize'))
        
        #now perform the annealing depending on the chosen local minimizer
        #default scipy.optimize.minimize functions
        #parametrization=annealing(costfunction,param_values,minimizer_kwargs={'method':'SLSQP','args':[configfile]})
        if (method=='TNC') or (method=='L-BFGS-B') or (method=='SLSQP'):
            minimizer_kwargs={'method':method,'bounds':bnds,'options':minopts,
                      'args':[configfile]}
            print(minimizer_kwargs)
            parametrization=annealing(costfunction, param_values, 
                                      minimizer_kwargs=minimizer_kwargs,
                                      niter=annealinginpt.getint('niter'),
                                      T=annealinginpt.getfloat('T'),
                                      stepsize=annealinginpt.getfloat('stepsize'),
                                      interval=annealinginpt.getint('interval'),
                                      niter_success=annealinginpt.getint('niter_success'),
                                      take_step=bounded_step)
        
        #trf
        if method=='trf':
            minimizer_kwargs={'method':myleastsq,'bounds':(lb,ub),'options':minopts,
                      'args':[configfile]}
            parametrization=annealing(residuals, param_values, 
                                      minimizer_kwargs=minimizer_kwargs,
                                      niter=annealinginpt.getint('niter'),
                                      T=annealinginpt.getfloat('T'),
                                      stepsize=annealinginpt.getfloat('stepsize'),
                                      interval=annealinginpt.getint('interval'),
                                      niter_success=annealinginpt.getint('niter_success'),
                                      take_step=bounded_step)
    elif psotest:
        pos,costpso=pso(costfunction,lb,ub, args=[configfile],
                        swarmsize=psoopts.getint('swarmsize'),
                        maxiter=psoopts.getint('maxiter'),
                        minstep=psoopts.getfloat('minstep'),
                        minfunc=psoopts.getfloat('minfunc'),
                        omega=psoopts.getfloat('omega'),
                        phip=psoopts.getfloat('phip'),
                        phig=psoopts.getfloat('phig'),
                        debug=psoopts.getboolean('debug'))
        return pos,costpso
        
    else:
            
        if method=='trf':
    
            #options={'diff_step':0.01,'max_nfev':1000}
            parametrization=minimize(residuals,param_values, args=configfile,
                                     bounds=(lb,ub), method=myleastsq,
                                     options=minopts)
        
        #here go the defaults from scipy.optimize.minimize
        elif (method=='TNC') or (method=='L-BFGS-B') or (method=='SLSQP'):
            parametrization=minimize(costfunction, param_values, args=configfile,
                                     bounds=bnds,method=method,options=minopts)
        
            
        
        
    return parametrization
if __name__ == "__main__":
    print('Module loaded')