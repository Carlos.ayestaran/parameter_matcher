# parameter-matcher.py

This is a Python 3 module designed for quick and highly customizable force field (FF) parametrization through energy-matching for molecular dynamics (MD).
In particular, this was developed for 'interface' potentials where a number of molecules are adsorbed on a surface, and MD calculations are performed in LAMMPS.
As such, it is not immediately transferrable to other systems, but it should be easily modifiable if required.
The user should have a number first-principles calculations performed on several configurations of the system and the corresponding molecules-surface energy interaction, and then same configurations ported to LAMMPS.
From here, the goal is to parametrise and obtain a FF that reproduces said energies, which should be more accurate than transferrable FFs from the literature.

There are basically two things that can be done with this module:
* Sensitivity test: study which parameters affect the energies the most, i.e. how a relatively small x% variation in one parameter reflects on a y% variation in the energies.
This should help the user sort which parameters should be fitted first in case there are more parameters than first-principles calculations available (and so avoid overfitting).
* Parametrisation: run an optimisation modifying a set of parameters to match the first-principles energies.

The instructions that follow take the reader through how this process works. They are tailored for a system as explained above, but should be general.

Note this module was written by a Python and LAMMPS amateur (yours truly), and so it may have its shortcomings. At the moment, it does not handle errors, so if one should rise, it will be Python itself pointing it out and saying something is wrong, but it will not tell you if and where there is a mistake in the inputs or somewhere else. Otherwise, I believe I have made it very easy to use and as optimised as I believe it can be. The options included here will allow anyone to easily go beyond the literature FF as a preamble to any MD simulation.

This module was derived from my MSc Thesis at the Imperial College London's TSM CDT.

## Getting Started

### Prerequisites

Python 3 and a LAMMPS distribution. The module **does not**  work with Python 2.

Modules that should be installed for this one to work:

* subprocess
* configparser
* numpy
* scipy
* tempfile
* shutil
* os
* pyswarm
* ast

### Installing

Literally just import it with Python.

# Instructions

## Preparing the files
The user should have a set of input files prepared for using with this module, all in the same folder.

### LAMMPS inputs and includefiles
First, naturally, the LAMMPS files, which will be calculating the interfacial interaction energies for each configuration.
There should be a 'compute' defined that performs said calculation, i.e., 'c_adsenertot'. Then, the FF parameters should be stored in a separate file for each configuration (includefiles), which should be correspondingly called with the 'include'. Finally, a 'run 0' command to, of course, perform the compute, which will be dropped in the logfile.

The includefiles should be structured in a particular way to be handled by Python:

Suppose we have defined a pair_style lj/cut. Suppose we have two atoms, Fe and O, with indices 1 and 2, and we want to modify their epsilon and sigma parameters. We name them OFeeps and OFesig. The includefiles should first have the lines:

```
variable OFeeps equal [value1]
variable OFesig equal [value2]
```
and further below

```
pair_coeff 1 2 lj/cut ${OFeeps} ${OFesig}
```
This way, the includefiles can be parsed from Python, find the parameter names, modify the values in the 'variable OFe...' lines and then the pair_coeffs changed.

The reason for using includefiles instead of stating the commands directly in the LAMMPS inputs is because this way they can be handled separately. Once the LAMMPS inputs are settled, they do not need to be touched again, whereas the lines including the stuff in the includefiles probably have to be messed with repeatedly. They can be cumbersome depending on the system and interactions, so it is easier to write and parse them through Python scripts and leave the LAMMPS inputs clean. Plus, that way one can have several includefiles for each LAMMPS input, but only need one of the latter. Finally, if several LAMMPS inputs have the atoms' IDs sorted in the same way, they can share an includefile.

### Configfile
Second, a configuration file (configfile). This file will be read in Python through the configparse module and will have information on how to call LAMMPS from the shell, optimiser options... . See the provided example configfile, that I advise to download.

The configfile has 5 sections. The first, 'DEFAULT', has backup values for the others and I discourage messing with it.

The second, 'user', has 4 keys:
* logfile: name of the LAMMPS logfile. By default, log.lammps, but can be changed in case one wants to separately store different runs.
* prefix: prefix to call LAMMPS from the shell. This is, LAMMPS will be called as in '[prefix] input.in'. An example of prefix is '/usr/local/Cellar/lammps/2017-08-11_4/bin/lmp_serial -l log.lammps -sc none -in'. Note that in the example configfile, there is a placeholder for the logfile name, which will include what is specified in the logfile key. I advise including the -sc none command to avoid printing to the screen, which will have no effect whatsoever other than taking a sizeable amount of time. I also advise calling a serial LAMMPS executable instead of a parallelized one. The reason is, parallelization actually hinders the efficiency of the calculations: it takes longer to copy stuff between nodes than to perform the single-point calculations, which are very quick (just one timestep!).
* inputfile: name of the inputfile.
* energyname: name of the interaction energy compute (e.g., c_adsenertot), to be parsed and recovered from the logfile.

The 'minimization' section has two keys and handles the Quasi-Newtonian algorithms to perform local, bounded optimisation:
* method: either 'L-BFGS-B', 'SLSQP', 'TNC', or 'trf'. The first three are included in the [scipy.optimize.minimize module](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html), while the latter is included in [scipy.optimize.least_squares](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html). See the corresponding documentation for more details.
* minimizer_options: options to be passed to the minimizer, in the shape of a Python dict. Again, see the respective documentations for more details.

The 'annealing' section handles an optional simulated annealing minimization algorithm, called from [scipy.optimize.basinhopping](https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.optimize.basinhopping.html). Simulated annealing is a method to go beyond the local minimization provided by Quasi-Newtonian algorithms by performing local minimizations coupled with random jumps. The local minimizer and its options are the ones specified in the 'minimization' section. If the 'annealing' key is False, only a local minimizations are performed (in other words, no annealing). The basinhopping takes a number of options, specified in the example configfile. They are self-explanatory, but see the scipy documentation for more details.

The 'particle swarm' section handles an optional particle swarm optimization, which is performed by making use of the [pso](https://pythonhosted.org/pyswarm/) function in the pyswarm module. [This algorithm](https://en.wikipedia.org/wiki/Particle_swarm_optimization) sets a given number of 'particles' in the parameter space, which move as a 'swarm' (interacting with each other) trying to find the parameters that give the best match. If this minimization is chosen (by setting the 'pso' key to True), the others will not be employed.


### Inputfile
Finally, there is another input file (inputfile) that will have information on the names of the LAMMPS configuration inputs, the associated energies, includefiles and weights; and the parameters to match, with initial guesses, bounds and weights.

The inputfile is divided in two sections, separated by a line with at least one '#' character.

The first section has the information on the LAMMPS inputs and configurations. It consists of several lines structured as follows

```
[LAMMPS input] [energy per molecule] [#molecules] [configuration weight]
```
For example, one of the lines could be

```
LAMMPS_1.in -51.52 16 1
```
Where we are telling it that there is a LAMMPS input called 'LAMMPS_1.in', associated with a first-principles calculation that gives an energy of -51.52 per molecule (whatever the units used in LAMMPS are), that has 16 molecules adsorbed and an associated weight of 1 in the fitting.

The reason for having the energy per molecule is because different files with different number of molecules can be employed. A file with more molecules will have larger energy values and larger absolute errors associated with it, so the fitting has to be performed on a per-molecule basis so as not to be skewed. Also, different weights for each configuration can be allowed in the fitting; for example, if different configurations are of intrinsically different energy scales (one with molecules very close to the surface and another with molecules very far away, for example), and again, associated with different absolute errors scales, weighting each accordingly can avoid a bias in the fitting.

The second section has the names given to the parameters in the includefiles (in the example above, 'OFeeps' and 'OFesig'), followed by their initial/default values, a lower bound, an upper bound and a weight:

```
[parameter name] [initial value] [lower bound] [upper bound] [weight]
```
For example

```
Feeps 0.3  0.15 7.0 0
```

The parameter names, of course, should be the name as in the includefiles, and the remaining values are used in the fitting.

The reason for including weights in the parameters is because they can be used as constrains in the fitting: maybe one wants to penalise optimisations that step away from the initial values, so constrains can be used in the fitting to penalise it proportionally to the weights.

## Using the module

### Sensitivity test

The function 'sensitivity_est' can suggest the user what parameters to should be prioritised in the fitting.

The way it works is by going through all the parameters in the inputfile and seeing how the energies are affected when small variations are made in the initial values. More specifically:

1. Pick a parameter.
2. Pick a LAMMPS input.
3. Call LAMMPS and store the associated original energy with the initial parameter value (specified in the inputfile).
4. Variate the original value by +0.1, and calculate the new energy.
5. Store the coefficient between the **relative** variation in the energy and the **relative** variation in the parameter. If the initial value of the parameter was <0.1, the variation is taken to be 100%.
6. Go back to 2 and repeat for the remaining LAMMPS inputs. Add the absolute value of all the coefficients in step 5.
7. Go back to step 1 and repeat for the remaining parameters specified in the inputfile.

The function will then sort them according to which has the highest effect on the energies and make a suggestion to prioritise them in the fitting. It also unpacks a list with the parameters sorted (from higher effect in the energies to lower) and an array with the associated variations.

The syntax is just
```
sensitivity_est('configfile')
```
where 'configfile' is just a string with the configfile name.

### FF optimization

The function 'matcher' will perform the FF optimization, for the files, parameters, bounds, weights.. in the inputfile, and using the optimization methods as prescribed in the configfile, explained above. It serves as a bridge between all the algorithms included in this module. Separately, each require remarkably different and long syntaxes and are cumbersome to implement, modify and test, specially those derived from different modules. By having this function, the goal is to simplify this process. Simply write
```
matcher('configfile')
```

and obtain the optimized result. For the annealing and Quasi-Newtonian algorithms, the function returns an [OptimizeResult](https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.optimize.OptimizeResult.html#scipy.optimize.OptimizeResult) Python object. If the particle swarm optimization algorithm is used, the function returns an array with the values of the optimized result and a float with the associated cost. (**Quick note**: for those unfamiliar, cost is the value to minimize during an optimization. For a 'vanilla' optimization with no weights, cost is the sum of squares of the differences between the first-principles and the LAMMPS energies; when weights are included, the differences are weighted accordingly, and also includes the penalties associated with stepping away from the initial parameter values).

I advise the user to test different methods and different options on each, to see how each perform for their system. For example, I have found trf was usually very good at finding a minimum and robust to weird initial guesses, but slower than others, so not suitable to implement with annealing. TNC can be efficient and robust if options (such as step size and tolerance) are tuned. Particle swarm is to get an initial guess if one has none, and then one can apply other algorithms more thoroughly on it.

To give an idea, matching 10 parameters between 13 configurations (each taking ~0.2s in LAMMPS to calculate the single-point energy) usually took me <30 min for a standard Quasi-Newtonian minimization. Time will naturally depend on the precision desired and initial guesses provided.

### Other uses

The module includes several other functions that are used by the two described above. A couple useful ones are
* getmethevalue(logfile, compute): a simple function to parse LAMMPS log files and recovers the value of a compute. Can be useful to recover computes other than the molecule-surface interaction energy. 'logfile' and 'compute' should both be strings.
* costfunction(param_values,configfile): calculates and prints the residuals and cost associated with a given set of parameter values. Useful to get them straight away to check, for example, how different sets of parameters from the literature perform. It is called by the 'matcher' function. param_values should be an array, and configfile a string.

### Examples

An example configfile and inputfiles are included, together with two LAMMPS configurations to test, already prepared. The two LAMMPS configurations are of adsorbed hexanamide molecules on an iron oxide surface. The interaction between the atoms in the hexanamides and the surface is described by LJ or Morse interactions, depending on the atoms. 
As stated in the inputfile, the names of the parameters to match are 'Feeps' and 'Fesig', which are the LJ parameters for the Fe in the surface. These parameters affect the LJ interactions with other atoms through geometric mixing rules. Not all atoms are affected by the LJ parameters, but it will be a bit long to explain the specifities.
For the sake of simplicity, suffice it to say that all the C and H atoms in the tailgroup interact with the Fe atoms through LJ, while other may not. I will provide a link to my dissertation if possible in the future, where this is explained in detail.

The examples, thus, provide only two parameters to match with two LAMMPS geometries, and with already good initial guesses, so any (local) minimization method should finish in a few seconds. 
Simply download them and run the sensitivity_est and matcher functions accordingly on the same folder, and you should be set to go.
Feel free to experiment with the different minimization methods and to tweak with precission and other options, or to change the initial guesses and to see how that affects the minimization behaviour. 
As mentioned above, the configfile has some comments suggesting options for minimizers, so you should start with those. I have found the options usually have to be very finely tuned, and the minimizers worked very poorly if not done carefully. But this system is simple enough to stand a poorer tuning. 

## Further work

By far, the longest time during the matching is taken during the LAMMPS single-energy calculations.
As explained above, even though LAMMPS allows for parallelization, it is of no use in calculations as short as these ones, since it takes longer just to (I believe) copy information between nodes. Still, it is the bottleneck of the process.

Perhaps parallelization could be implemented in a different way, where each LAMMPS evaluation is performed serially on each node, thus having several nodes simultaneously running separate LAMMPS calculations. This way, the total wall time would be divided by the number of nodes, in the best-case scenario. Alas, this requires more expertise that I can provide, at least for the time being.


## Author

**Carlos Ayestarán Latorre**, Tribology Group, Imperial College London

All questions and suggestions will be gladly received at carlos.ayestaran-latorre17@imperial.ac.uk

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
